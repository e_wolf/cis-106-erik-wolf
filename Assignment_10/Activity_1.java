//Create a program that asks the user to enter grade scores. 
// Start by asking the user how many scores they would like to enter. 
// Then use a loop to request each score and add it to a total. 
// Finally, calculate and display the average for the entered scores. 

// Using separate subroutines/functions/methods, implement the loop control structure based on a while loop in one subroutine and a for loop in the other subroutine. Use separate subroutines/functions/methods for input, processing, and output. Avoid global variables by passing parameters and returning results.

import java.util.*;
import java.lang.Math;
import java.util.Scanner;

class Main 
{
	private static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args) 
    {
    	int gradeCount;
    	int forAverageGrade;
    	int whileAverageGrade;

    	gradeCount = NumberofGrades();
    	forAverageGrade = ForLoopGradeBook(gradeCount);
    	whileAverageGrade = WhileLoopGradeBook(gradeCount);
		displayResults(forAverageGrade, whileAverageGrade);
    }
    
    // get number of grades to be entered
    private static int NumberofGrades()
    {
    	int gradeCount;
    	boolean isInt;
    	
    	gradeCount = 0;
    	
    	do
    	{
    		System.out.println("How many grades must be entered?");
			if(input.hasNextInt())
			{
    			gradeCount = input.nextInt();
    			isInt = true;
			}
			else
			{
				System.out.println("That is not a valid integer.");
	        	System.out.println();
	        	isInt = false;
	        	input.next();
			}
    	}
    	while(!(isInt));
    	
    	return gradeCount;
    }
    
    // get score for grades 
    private static int getScore()
    {
		int score;
		boolean isInt;
    	
    	score = 0;
    	
    	do
    	{
    		System.out.println("Enter score:");
			if(input.hasNextInt())
			{
    			score = input.nextInt();
    			isInt = true;
			}
			else
			{
				System.out.println("That is not a valid integer.");
	        	System.out.println();
	        	isInt = false;
	        	input.next();
			}
    	}
    	while(!(isInt));
		
		return score;
    }
    
    //for-loop example of calculating average
    private static int ForLoopGradeBook(int gradeCount)
    {
		int i;
    	int total;
    	int averageGrade;
    	int score;

    	total = 0;
    	System.out.println("For Loop:");
    	System.out.println("");
		for (i = 0; i < gradeCount; i += 1)
		{
			score = getScore();
 			total = total + score;
		}
		
		averageGrade = total / gradeCount;
		System.out.println("");	
		
    	return averageGrade;
    }
    
    //while-loop example of calculating average  
    private static int WhileLoopGradeBook(int gradeCount)
    {
		int i;
    	int total;
    	int averageGrade;
    	int score;
    	int count;
		
		count = 0;
    	total = 0;
    	System.out.println("While Loop:");
    	System.out.println("");
        
    	while (count < gradeCount)
		{
			score = getScore();
 			total = total + score;
 			count = count + 1;
		}
		
		averageGrade = total / gradeCount;
		System.out.println("");
		
    	return averageGrade;
    }
    
    //output
    private static void displayResults(int forAverageGrade, int whileAverageGrade)
    {
    	System.out.println("This Student has an Average Score of: " + " for: " + forAverageGrade + ", while: " + whileAverageGrade);
    }
}
