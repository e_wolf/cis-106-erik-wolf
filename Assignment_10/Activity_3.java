//Create a program that uses nested loops to generate a multiplication table. Rather than simply creating a 10 by 10 table, ask the user to enter the starting and ending values.

import java.util.*;
import java.lang.Math;
import java.util.Scanner;

class Main 
{
    private static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args) 
    {
        int lastValue;
        int startLength;
        
        startLength = getStartValue();
        lastValue = getLastValue();
        Table(lastValue, startLength);
    }

    private static int getStartValue() 
    {
        int startLength;
        boolean isInt;
        
        startLength = 0;
        
        do
        {
        System.out.println("Enter starting value:");
        	
	        if(input.hasNextInt())
	        {
	        	startLength = input.nextInt();
	        	isInt = true;
	        }
	        
	        else
	        {
	        	System.out.println("That is not a valid integer.");
	        	System.out.println();
	        	isInt = false;
	        	input.next();
	        }
        }
        while(!(isInt));
        
        return startLength;
    }

    private static int getLastValue() 
    {
        int lastValue;
        boolean isInt;
        
        lastValue = 0;
        
        do
        {
        	
        System.out.println("Enter final value:");
        	
	        if(input.hasNextInt())
	        {
	        	lastValue = input.nextInt();
	        	isInt = true;
	        }
	        
	        else
	        {
	        	System.out.println("That is not a valid integer.");
	        	System.out.println();
	        	isInt = false;
	        	input.next();
	        }
        }
        while(!(isInt));
        
        return lastValue;
    }
    
    //generates Table
    private static void Table(int lastValue, int startLength) 
    {
        int operation;
        int i;
        int result;
        int a;
        
        OutputHeader(startLength, lastValue);
        
        if(startLength < lastValue)
        {
	        for (i = startLength; i <= lastValue; i += 1) 
	        {
	            OutputColumn(i);
	            
	            for (a = startLength; a <= lastValue; a += 1)
	            {
	            	result = i * a;
	            	OutputRow(result);
	            }	            
	            OutputLine();
	        }
        }
        
        else
        {
        	for (i = startLength; i >= lastValue; i -= 1) 
	        {
	            OutputColumn(i);
	            
	            for (a = startLength; a >= lastValue; a -= 1)
	            {
	            	result = i * a;
	            	OutputRow(result);
	            }	            
	            OutputLine();
	        }
        }
    }
    
    private static void OutputLine()
    {
    	System.out.println();
    }
    
    // sets first line of Table
    private static void OutputHeader(int startLength, int lastValue) 
    {
		System.out.format("      ");
		if(startLength < lastValue)
		{
	    	for(int i = startLength; i<=lastValue;i++ ) 
	    	{
	            System.out.format("%4d",i);
	        }
		}
		else
		{
			for(int i = startLength; i >= lastValue; i--) 
	    	{
	            System.out.format("%4d",i);
	        }
		}
        System.out.println();
    }
    
    private static void OutputRow(int result) 
    {
   	
        System.out.format("%4d", result);
    }
    
    private static void OutputColumn(int i) 
    {
        System.out.format("%4d |", i);
    }

    
}