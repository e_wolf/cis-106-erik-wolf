// Write a program that uses the Nilakantha series to calculate Pi based on a given number of iterations entered by the user. How many iterations are necessary to accurately calculate Pi to 15 digits?

// PI accurate to 15 digit     3.14159265358979

import java.util.*;
import java.lang.Math;
import java.util.Scanner;

class Main 
{
	private static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args) 
    {
		double count = 1;
		double pi = 3;
		double hold = 0;

		while (hold != pi)
		{
		    hold = pi;

		    if (count % 2 == 1)
		    {
    			pi = pi + (4 / ((count * 2) * (count * 2 + 1) * (count * 2 + 2)));
		    }
		    else
		    {
    			pi = pi - (4 / ((count * 2) * (count * 2 + 1) * (count * 2 + 2)));
		    }

			System.out.println("count: " + count);
			count++;
			System.out.println("pi: " + pi);
		}
		System.out.println(count);
	}
}
