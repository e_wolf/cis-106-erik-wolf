import java.util.*;

class Main 
{
	private static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args) 
    {
        String statement; 
        statement = input.nextLine();
        
        int length = statement.length();
        char[] tempCharArray = new char[length];
        char[] charArray = new char[length];
        
        // sets an array
        for (int i = 0; i < length; i++) 
        {
            tempCharArray[i] = statement.charAt(i);
        } 
       
        // reverse array
        for (int j = 0; j < length; j++) 
        {
            charArray[j] = tempCharArray[length - 1 - j];
        }
        
        String reverseStatement = new String(charArray);
    	System.out.println(reverseStatement.trim());
    }
}