/*
Create a program that asks the user to enter grade scores. 
Start by asking the user how many scores they would like to enter. 
Then use a loop to request each score and add it to a dynamic array. 
After the scores are entered, calculate and display the high, low, and average for the entered scores.
Use separate subroutines/functions/methods to implement input, each type of processing, and output. 
Avoid global variables by passing parameters and returning results.
*/

import java.util.*;
import java.lang.Math;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;

class Main 
{
	private static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args) 
    {
    	int size;
    	int high;
    	int low;
    	int average;
    	int list;
    	
    	size = Size();
    	
    	List<Integer> myList = new ArrayList<Integer>();
    	System.out.println("Enter Scores:");
		
		//add values to elements
    	for(int i = 0; i < size; i++)
    	{
    		myList.add(input.nextInt());
    	}
		
    	average = Average(myList, size);
    	high = High(myList, size);
    	low = Low(myList, size);
    	Output(average, high, low);

    }
    
    public static int Size(){
    	int size;
    	
    	System.out.println("How many scores?");
    	
    	size = input.nextInt();
    	
    	return size;
    }	
	
    public static int High(List<Integer> myList, int size)
    {
    	//calculate High
    	int high;
        
        high = myList.get(0);
        
        for (int i = 1 ; i <= size - 1 ; i += 1) 
        {
            if (myList.get(i) > high) {
                high = myList.get(i);
            }
        }
        
        return high;
    }
    
    public static int Low(List<Integer> myList, int size)
    {
    	//calculate low
    	int low;
        
        low = myList.get(0);
        
        for (int i = 1 ; i <= size - 1 ; i += 1) 
        {
            if (myList.get(i) < low) {
                low = myList.get(i);
            }
        }
        
        return low;
    }
        
    public static int Average(List<Integer> myList, int size)
    {	
    	//calculate average
    	int average;
        int total;
        
        total = 0;
        
        for (int i = 0 ; i < size ; i += 1) {
            total = total + myList.get(i);
        }
        
        average = (total / size);
        
        return average;
    }
    
    public static void Output(int average, int high, int low)
    {
    	System.out.println("Highest Score: " + high);
    	System.out.println("Lowest Score: " + low);
    	System.out.println("Average Score: " + average);
    }
}