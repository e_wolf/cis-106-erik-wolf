import java.util.Random;

public class Main{
	
	public static void main(String[] args)
	{
		int changeDoor = 0;
		int holdDoor = 0;
		Random random = new Random();
		
		for(int i = 0; i < 100; i++ )
		{
			int[] doors = {0,0,0};
			doors[random.nextInt(3)] = 1;
			int choice = random.nextInt(3); 
			int shown;
			
			do
			{
				shown = random.nextInt(3);
			
			}
			while(doors[shown] == 1 || shown == choice);
 
			holdDoor += doors[choice];
			changeDoor += doors[3 - choice - shown];
		}
		System.out.println("Changing doors results in a " + changeDoor + "% chance of winning.");
		System.out.println("Holding on to the first door is only a " + holdDoor + "% chance of winning");
	}
}