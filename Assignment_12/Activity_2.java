/*
Create a program that asks the user for a year; and then calculate whether or not the given year is a leap year. 
Build an array where each entry is the number of days in the corresponding month (January = 31; February = 28/29; March = 31; etc.). 
Build a parallel string array with the names of each month. 
Then ask the user to enter a month number; and look up the corresponding month name and number of days and display the information. 
Continue accepting input and displaying results until the user enters a number less than 1 or greater than 12. 
Use separate subroutines/functions/methods to implement input; each type of processing; and output. 
Avoid global variables by passing parameters and returning results.
*/

import java.util.*;
import java.lang.Math;
import java.util.Scanner;
import java.util.Arrays;

class Main 
{
	private static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args) 
    {
    	int[] commonYear = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    	int[] leapYear = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    	String[] MonthName = {"January", "February", "March",
								"April", "May", "June",
								"July", "August", "September",
								"October", "November", "December"};
    	int year;
    	int getMonth;
    	String month;
    	int monthDays;
    	int catalog;
    	
    	do{
		getMonth = GetMonth();
		month = SetMonthName(getMonth, MonthName);
		year = GetYear();
		monthDays = SetMonthDays(getMonth, year, commonYear, leapYear);
    	Output(month, monthDays, year);
    	} while(getMonth > 0 && getMonth < 12);
    	
    	System.exit(1);
		
    }
    
    private static String SetMonthName(int getMonth, String[] MonthName)
    {
    	String monthName;
    	
    	monthName = MonthName[getMonth - 1];
    	
    	return monthName;
    	
    }
    
    private static int SetMonthDays(int getMonth, int year, int[] commonYear, int[] leapYear)
    {
    	int monthDays;
    	
    	if((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))){
    		monthDays = leapYear[getMonth - 1];
    	}
    	else{
    		monthDays = commonYear[getMonth - 1];
    	}
    	
    	return monthDays;
    }
    
    private static int GetMonth() 
    {
    	int getMonth;
    	
    	System.out.println("Pick a month 1-12:");
    	getMonth = input.nextInt();
    	
    	return getMonth;
    }
    
    private static int GetYear() 
    {
    	int year;
    	
    	System.out.println("What Year is it?");
    	year = input.nextInt();
    	
    	return year;
    }
    
    private static void Output(String month, int monthDays, int year)
    {
    	System.out.println("In the year " + year + ", " + month + " has " + monthDays + " days.");
    }
}