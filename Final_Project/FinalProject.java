import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
/*
 *Build parallel arrays for the plant items,
 * with each array containing the plant's full name, zone, light, and price, respectively. Availability will be ignored. After reading the data and building the arrays, display the plant items similar to the following:
 * <p>
 * common name (botanical name) - zone - light - price
 * <p>
 * At the bottom, display the total number of items in the catalog and the average price per item similar to:
 * <p>
 * 0 items - $0.00 average price
 * <p>
 *
 * Created by Erik on 12/2/2017.
 *
 * Edit 12/8/2017 - Working on Parsing XML doc using XPath, currently Prints Root Node. Next step is itrerate each
 *
 *Edit 12/8/2017 - document built, 1 node iterate complete by .getElementsByTagName() method since tag element is known.
 */

public class FinalProject
{
    public static void main(String[] args) throws Exception 
    {

        try
        {
            Document doc = DocumentBuilder();
            double sum = NodeBuilder(doc);
            double averagePrice = CalculateAverage(sum, doc);
            int itemCount = ItemCount(doc);
            OutputAverage(averagePrice, itemCount);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Document DocumentBuilder() throws Exception
    {
        //Get Doc
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbFactory.newDocumentBuilder();

        //Parse Doc
        Document doc = db.parse(new File("Plant_Catalog.xml"));

        //Normalize Doc
        doc.getDocumentElement().normalize();

        return doc;
    }
    private static double NodeBuilder(Document doc)
    {
        double sum = 0;
        //Set Root Node
        Element rootNode = doc.getDocumentElement();
        System.out.println(rootNode.getNodeName());

        //Get Plant Element into list
        NodeList nodeList = doc.getElementsByTagName("PLANT");
        for (int i = 0; i < nodeList.getLength(); i++)
        {
            Node node = nodeList.item(i);

            System.out.println("-------------------");

            if (node.getNodeType() == Node.ELEMENT_NODE)
            {
                //Print nodelist
                Element nodeElement = (Element) node;
                System.out.println("Name: " + nodeElement.getElementsByTagName("COMMON").item(0).getTextContent());
                System.out.println("Zone: " + nodeElement.getElementsByTagName("ZONE").item(0).getTextContent());
                System.out.println("Light: " + nodeElement.getElementsByTagName("LIGHT").item(0).getTextContent());
                System.out.println("Price: " + nodeElement.getElementsByTagName("PRICE").item(0).getTextContent());
                sum += Double.parseDouble(nodeElement.getElementsByTagName("PRICE").item(0).getTextContent().replaceAll("[$,]", ""));
            }
        }
        return sum;
    }
    
    //get Plant count
    private static int ItemCount(Document doc)
    {
        NodeList node = doc.getElementsByTagName("PLANT");
        int  itemCount = node.getLength();

        return itemCount;
    }
    
    //average out the price per plant
    private static double CalculateAverage(double sum, Document doc)
    {

        NodeList node = doc.getElementsByTagName("PRICE");
        double  averagePrice = sum / node.getLength();

        return averagePrice;
    }

    private static void OutputAverage(double averagePrice, int itemCount)
    {
        //Output Count and format average price to 2 decimals
        System.out.println("-------------------");
        System.out.printf("Average Price of " + itemCount + " Plants: $%.2f", averagePrice);
    }

}