// Introduction My name is Erik Wolf, I recently jumped back into school after taking break. I have some experience with programming but in my opinion a weak foundation. I'm taking this course and three other courses as well. In the spring I will be applying to UIC for my Bachelor. I look forward to building a strong foundation by taking this course and future courses.

// Programming Language
Java, because it is the most widely supported and used language available. My goal for learning this language is to creat powerful enterprise applications that will help other people.

// IDEs 
IDE stands for Integrated Development Environment, it is a software application the consists of a source code editor, automation tools, and debugger. They can be extremely powerful in collaborating on multiple areas of a project via source control services like BitBucket, GitHub, etc.

// Activity 3 
This activity was focused on incorporating structure and comments so that code can be easier to follow. Naming conventions are a big help in this process. Also, declaring variable at the top of the page is a clean way of managing variables that will be used.

//Assignment 4 
I used conditional statments for Activity-6 because I felt that giving the user a choice on whether they are looking for a triangle or circle. More shapes can easily be applied to the if/else conditions. I learned how to use .equalsIgnoreCase which covers a spectrum of errors so I can capture "CiRcLe" and not limit myself to only capturing "circle" or "Circle".

//Assignment 5 
This assignment I seperated my logic into several functions for structuring programs correctly so that when I write a larger program, I will know how to structure it properly. With each function seperate, I can call the conversion functions only when I need to, rather than calling one function with every calculation.

//Assignment 6 
For this assignment I focused on calling functions based on conditions passed through via user input. This provides seperation and places most Input logic within one function, where capturing the input then passes the task to the called function.

//Assignment 7 
Felt very similiar to assignment 6, I am looking forward to the day I can step away from flowgorithm because calling functions and passing parameters can be confusing in flowgorithm. Overall It has helped when to declare vairables and with passing parameters.

//Assignment 8
This assignment felt very straight forward besides a few tricky parts.  These tricky parts were that using Switch Case statements were more powerful for checking string inputs.  But in validating user input for the number, I found myself using do-while loops with a Scanner input validation method .hasNextDouble()..

//Assignment 9
Loops can be challenging at first but I view them and how many iterations are required for this process to be complete.  It is a condition that runs until the condition is met.  

//Assignment 10
This activity focused on utilizing loops (for, while, do) within many different contexts.  Using loops to calculate averages, using nested loops to output multiple calculations, and rounding to nth decimal places.

//Assignment 11
In Assignment 11, I accomplished three ways of working with araays. First, averaging all values indexed. Also, parsing through the array and grabbing the lowest and/or highest value. 

//Assignment 12
Utilizing dynamic arraya and Interfaces was something I have not done for quite some time.  This was a great help to almost locking down Interfaces and type casting.

//Assignment 14
OPening and reading the file was a challenge in Java because I was running into problems with exception handling.  Turns out that Java requires Try{} catch{} block when closing a file.  I switched back to intelliJ because I was in desperate need of a debugger.  I did the assignment in Cloud9 but ran into problems and found out that Cloud9 does not support Java Debugging/Breakpoints.

//Final Project
I learned the importance of dynamic arrays and how to fill them accordingly.  Looking up how to use XPath was a lot of fun, very powerful when working with XML documents and super easy.  Monty Hall problem is something I'll definitely remember, it was a very challenging working out the logic.  This course has taught me more about planning and organizing my time which I am very poor with.  Staying on top of discussion posts and submitting assignments on time was challenging with the busy schedule this semester.  Thank you for everything you are a wonderful instructor and extremely knowledgeable.  If I do not get into the schools of my choice I will definitely look at taking a course with you in the Spring. 
