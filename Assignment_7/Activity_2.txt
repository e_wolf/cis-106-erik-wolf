Module main()
    // Create a program to prompt the user for hours and rate per hour to compute gross pay (hours * rate). Include a calculation to give 1.5 times the hourly rate for any overtime (hours worked above 40 hours).[1] Use separate subroutines/functions/methods for input, processing, and output. Avoid global variables by passing parameters and returning results. 
    // 
    Declare Real hours
    
    Display "Enter number of hours worked"
    Input hours
    If hours <= 40 Then
        Call standardRate(hours)
    Else
        Call overtimeRate(hours)
    End If
End Module

Module overtimeRate(Real hours)
    Declare Real rate
    Declare Real overtimeRate
    Declare Real overtimeHours
    Declare Real grossPay
    
    Display "Enter current rate:"
    Input rate
    Set overtimeHours = hours - 40
    Set overtimeRate = 1.5 * rate
    Set grossPay = 40 * rate + overtimeHours * overtimeRate
    Display "Your Gross Pay this period is: $", grossPay
End Module

Module standardRate(Real hours)
    Declare Real rate
    Declare Real grossPay
    
    Display "Enter current rate:"
    Input rate
    Set grossPay = rate * hours
    Display "Your Gross Pay this period is: $", grossPay
End Module
