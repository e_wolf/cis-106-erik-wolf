import java.util.*;
import java.lang.Math;
import java.util.Scanner;

class JavaApplication {
    private static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args) {
        // Create a program to prompt the user for hours and rate per hour to compute gross pay (hours * rate). Include a calculation to give 1.5 times the hourly rate for any overtime (hours worked above 40 hours).[1] Use separate subroutines/functions/methods for input, processing, and output. Avoid global variables by passing parameters and returning results. 
        // 
        double hours;
        
        System.out.println("Enter number of hours worked");
        hours = input.nextDouble();
        
        //call function based on hours worked
        if (hours <= 40) {
            standardRate(hours);
        } else {
            overtimeRate(hours);
        }
    }
    
    //calculate overtime rate
    private static void overtimeRate(double hours) {
        double rate;
        double overtimeRate;
        double overtimeHours;
        double grossPay;
        
        System.out.println("Enter current rate:");
        rate = input.nextDouble();
        
        overtimeHours = hours - 40;
        overtimeRate = 1.5 * rate;
        grossPay = 40 * rate + overtimeHours * overtimeRate;
        
        System.out.println("Your Gross Pay this period is: $" + grossPay);
    }
    
    // normal rate for 0-40 hours
    private static void standardRate(double hours) {
        double rate;
        double grossPay;
        
        System.out.println("Enter current rate:");
        rate = input.nextDouble();
        
        grossPay = rate * hours;
        
        System.out.println("Your Gross Pay this period is: $" + grossPay);
    }
}
