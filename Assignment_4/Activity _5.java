// Create a program that asks the user for a distance in miles, and then calculate and display the distance in kilometers, meters, and centimeters.

import java.util.*;
import java.lang.Math;
import java.util.Scanner;


public class Main
{
	
    private static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args) 
    {
    	//variables
    	double miles;
    	double kilometers;
    	double meters;
    	double centimeters;
    	
    	//input miles
    	System.out.println("Enter distance in miles: ");
    	miles = input.nextDouble();
    	
    	//conversion
    	kilometers = miles * 1.609344;
    	meters = kilometers * 1000;
    	centimeters = meters * 100;
    	
    	//output conversions
    	System.out.println("Succesful conversion of " + miles + "mi to Metric:");
    	System.out.println(kilometers + "km");
    	System.out.println(meters + "m");
    	System.out.println(centimeters + "cm");
    }
}