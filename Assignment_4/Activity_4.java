// Create a program that asks the user how old they are in years, and then calculate and display their approximate age in months, days, hours, and seconds.
import java.util.Scanner;


public class Main 
{
	private static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args)
	{
		
		String userName;
		
		int yearsOld; 
		int monthsOld;
		int daysOld;
		int hoursOld;
		int secondsOld;
		int monthsPerYear = 12;
		int daysPerYear = 365;
		int hoursPerDay = 24;
		int secondsPerHour = 60;
		
		//Capture Name and Age in years of user
		System.out.print("What is your name?");
		userName = input.next();
		System.out.print("In years, how old are you " + userName + "?");
		yearsOld = input.nextInt();
		
		//Calculate user age in months, days, hours, seconds
		monthsOld = yearsOld * monthsPerYear;
		daysOld = yearsOld * daysPerYear; 
		hoursOld = daysOld * hoursPerDay;
		secondsOld = hoursOld * secondsPerHour;
		
		//Output calculated age
		System.out.print(userName + ", Your age converted to months is " + monthsOld + 
			" Months. In days, " + daysOld + 
			" Days. Here is how many hours you have lived, " + hoursOld + 
			" hours. Have you been productive? How about the amount of seconds you experienced, " + 
			secondsOld + " seconds.");
	}
}