import java.util.*;
import java.lang.Math;
import java.util.Scanner;

class Main {
    private static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args) {
    	
    	//variables
        double base;
        double height;
        double answer;
        double radius;
        String answerShape;
        String circle;
        String triangle;
        
        //capture user choice of shape
        System.out.println("Would you like to calculate the area of a triangle or circle?");
        answerShape = input.next();
        
        //conditional statement for circle or triangle
        if (answerShape.equalsIgnoreCase("circle"))
        {
        	//capture dimensions of circle
        	//calculate area of triangle
        	//output area
        	System.out.println("Please enter the radius of the circle");
        	radius = input.nextDouble();
        	answer = Math.PI * Math.pow(radius, 2);
        	System.out.println("The area of the circle is " + answer);
        } 
        else if (answerShape.equalsIgnoreCase("triangle"))
        {
            //capture dimensions of triangle
            //calculate area of triangle
            //output area
            System.out.println("Please enter the base of the triangle");
            base = input.nextDouble();
            System.out.println("Please enter the height of the triangle");
            height = input.nextDouble();
            answer = base * height * (double) 1 / 2;
            System.out.println("The area of the triangle is " + answer);
        }
   	    else
        {
            System.out.println("You must enter circle or triangle!");
            return;
        }
    }
}
