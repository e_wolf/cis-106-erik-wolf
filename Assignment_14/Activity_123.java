/**
 * Created by Erik on 11/30/2017.
 */
/*
Using the file above, create a program that displays high, low, and average scores based on input from the file. 
Verify that the file exists and then use string functions/methods to parse the file content and add each score to an array. 
Display the array contents and then calculate and display the high, low, and average score. 
Include error handling in case the file is formatted incorrectly. 
Use separate subroutines/functions/methods to implement each type of processing. 
Avoid global variables by passing parameters and returning results.
*/

// import org.jetbrains.annotations.Nullable;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

class Activity_123
{
    public static void main(String[] args)
    {
        String filename = "scores.txt";
        List<Integer> list = readFileAsList(filename);
        High(list);
        Low(list);
        Average(list);
        
    }

    // @Nullable
    public static List<Integer> readFileAsList(String filename)
    {
        List<Integer> newList = new ArrayList<>();
        BufferedReader reader = null;

        try
        {
            reader = new BufferedReader(new FileReader(filename));

            String line;

            while ((line = reader.readLine()) != null)
            {
                String newLine = line.substring(line.indexOf(":") + 1, line.length()).trim();
                newList.add(Integer.parseInt(newLine));
            }
            return newList;
        }
        catch (java.io.IOException e)
        {
            System.err.format("Exception occurred trying to read '%s'.", filename);
            e.printStackTrace();
            return null;
        }
        finally
        {
            try
            {
                if (reader != null)
                    reader.close();
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    public static void High(List<Integer> newList)
    {
        //calculate High
        int high;
        high =  newList.get(0);

        for (int i = 1 ; i < newList.size() ; i += 1)
        {
            if (newList.get(i) > high) {
                high = newList.get(i);
            }
        }
        System.out.println("Highest Score: " + high);
    }

    public static void Low(List<Integer> newList)
    {
        //calculate low
        int low;

        low = newList.get(0);

        for (int i = 1 ; i < newList.size() ; i += 1)
        {
            if (newList.get(i) < low) {
                low = newList.get(i);
            }
        }

        System.out.println("Lowest Score: " + low);
    }

    public static void Average(List<Integer> newList)
    {
        //calculate average
        int average;
        int total;

        total = 0;

        for (int i = 0 ; i < newList.size() ; i += 1) {
            total = total + newList.get(i);
        }

        average = (total / newList.size());

        System.out.println("Average Score: " + average);
    }
}
