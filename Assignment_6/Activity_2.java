// Create a program that asks the user how old they are in years, and then calculate and display their approximate age in months, days, hours, and seconds.
import java.util.Scanner;


public class Main 
{
	private static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args)
	{
		
		double ageInYears; 
		double ageInMonths;
		double ageInDays;
		double ageInHours;
		double ageInSeconds;
		
		//funtions called
		ageInYears = getUserAge();
		ageInMonths = yearToMonth(ageInYears);
		ageInDays = yearToDay(ageInYears);
		ageInHours = dayToHour(ageInDays);
		ageInSeconds = hourToSecond(ageInHours);
		displayResults(ageInYears, ageInMonths, ageInDays, ageInHours, ageInSeconds);

	}
	
	//capture user age
	public static double getUserAge()
	{
		double ageInYears;
		
		System.out.println("In years, how old are you?");
		ageInYears = input.nextDouble();
		
		return ageInYears;
	}
	
	private static double yearToMonth(double ageInYears)
	{
		double ageInMonths;
		
		ageInMonths = 12 * ageInYears;
		
		return ageInMonths;
		
	}
	
	private static double yearToDay(double ageInYears)
	{
		double ageInDays;
		
		ageInDays = 365 * ageInYears;
		
		return ageInDays;
		
	}
	
	private static double dayToHour(double ageInDays)
	{
		double ageInHours;
		
		ageInHours = 24 * ageInDays;
		
		return ageInHours;
		
	}
	
	private static double hourToSecond(double ageInHours)
	{
		double ageInSeconds;
		
		ageInSeconds = 3600 * ageInHours;
		
		return ageInSeconds;
		
	}
	
	//display results in console
	public static void displayResults(double ageInYears, double ageInMonths, double ageInDays, double ageInHours, double ageInSeconds)
	{
				//Output calculated age
		System.out.println("Your age converted to months is " + ageInMonths + " Months. In days, " + ageInDays + " Days. Here is how many hours you have lived, " + ageInHours + " hours. Have you been productive? How about the amount of seconds you experienced, " + ageInSeconds + " seconds.");
	}
	
}