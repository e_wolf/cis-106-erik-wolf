/* 
Create a program to prompt the user for hours and rate per hour and then compute gross pay (hours * rate).
Use separate subroutines/functions/methods for input, processing, and output. 
Avoid global variables by passing parameters and returning results.
*/

import java.util.*;
import java.lang.Math;
import java.util.Scanner;

public class Main {
    private static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args) 
    {
        // Create a program to prompt the user for hours and rate per hour and then compute gross pay
        int rate;
        int hours;
        int grossPay;
        
        rate = getRate();
        hours = getHours();
        grossPay = getGrossPay(rate, hours);
        displayResults(rate, hours, grossPay);
    }
    
    // get rate from user
    private static int getRate()
    {
    	int rate;
    	
    	System.out.println("Enter hourly rate: ");
        rate = input.nextInt();
        
        return rate;
    }
    
    // get hours from user
    private static int getHours()
    {
    	int hours;
    	
    	System.out.println("Enter hours worked: ");
        hours = input.nextInt();
        
        return hours;
    }
    
    // calculate input to determine Gross Pay (hours * rate)
    private static int getGrossPay(int rate, int hours)
    {
    	int grossPay;
    	
    	grossPay = (rate * hours);
    	
    	return grossPay;
    }
    
    //display results
    public static void displayResults(int rate, int hours, int grossPay)
    {
    	// Output grossPay
        System.out.println("Rate: $" + rate + "/hr, Hours: " + hours + "hrs, Gross Pay: $" + grossPay);
    }
}
