/*
Create a program that asks the user for the dimensions of different shapes and then calculate and display the area of the shapes. 
Use separate subroutines/functions/methods for input, processing, and output. 
Avoid global variables by passing parameters and returning results.
*/

import java.util.*;
import java.lang.Math;
import java.util.Scanner;

class Main {
    private static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args) 
    {
    	getShape();
    }
    
    // register user input for desired share, calling function based on user input
    public static void getShape()
    {
    	String choice;
    	
    	System.out.println("Would you like to determine the area of a Circle, Triangle, Trapezoid, or Ellipse?");
    	choice = input.nextLine();
    	
    	if (choice.equalsIgnoreCase("Circle"))
    	{
    		toCircle();
    	}
    	else if (choice.equalsIgnoreCase("Triangle"))
    	{
    		toTriangle();
    	}
    	else if (choice.equalsIgnoreCase("Trapezum"))
    	{
    		toTrapezoid();
    	}
    	else if (choice.equalsIgnoreCase("Ellipse"))
    	{
    		toEllipse();
    	}
    	else
    	{
    		toError();
    	}
    }
    
    //Area of circle with output
    private static void toCircle()
    {
    	double radius;
    	double answer;
    	
    	System.out.println("Please enter the radius of the circle:");
        radius = input.nextDouble();
        
        answer = Math.PI * Math.pow(radius, 2);
        
        System.out.println("The area of the circle is:" + answer);
    }
    
    //Area of Triangle with output
    private static void toTriangle()
    {
    	double height;
    	double base;
    	double answer;
    	
    	System.out.println("Please enter the base of the triangle:");
		base = input.nextDouble();
		System.out.println("Please enter the height of the triangle:");
		height = input.nextDouble();
		
		answer = base * height *(double) 1 / 2;
		
		System.out.println("The area of the triangle is :" + answer);
    }
    
    //Area of Trapezoid with output
    private static void toTrapezoid()
    {
		double baseA;
		double baseB;
		double altitude;
		double answer;
		
		System.out.println("Please enter first base:");
		baseA = input.nextDouble();
		System.out.println("Please enter second base:");
		baseB = input.nextDouble();
		System.out.println("Please enter altitude:");
		altitude = input.nextDouble();
		
		answer = (baseA + baseB) / 2 * altitude;
		
		System.out.println("The area of the Trapezoid is :" + answer);
    }
    
    //Area of Ellipse with output
    private static void toEllipse()
    {
    	double semiMinor;
    	double semiMajor;
    	double answer;
    	
    	System.out.println("Please enter the Semi-Major Axis:");
    	semiMajor = input.nextDouble();
    	System.out.println("Please enter the Semi-Minor Axis");
    	semiMinor = input.nextDouble();
    	
    	answer = Math.PI * semiMajor * semiMinor;
    	
    	System.out.println("The area of the Ellipse is:" + answer);
    }
    
    //simple error message
    private static void toError()
    {
    	System.out.println("Error, test more noob");
    }
    
}
