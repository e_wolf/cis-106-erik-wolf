/*
Create a program that asks the user how old they are in years.
Then ask the user if they would like to know how old they are in months, days, hours, or seconds.
Use if/else conditional statements to display their approximate age in the selected timeframe.
Perform all calculations using an AgeConverter class that accepts the age in years during initialization and has separate properties and methods to calculate and return the age in months, days, hours, and seconds.
Include data validation in the class and error handling in the main program.
*/
import java.util.Scanner;


class Activity_2
{
    private static Scanner input = new Scanner(System.in);

    public static void main(String[] args)
    {

        double ageInYears;
        double value;
        String choice;

        //functions called
        AgeConverter age = new AgeConverter();
        choice = getChoice();
        ageInYears = getUserAge();


        if (choice.equalsIgnoreCase("Months"))
        {
            value = age.yearToMonth(ageInYears);
            displayResults(value, choice);
        }
        else if (choice.equalsIgnoreCase("Days"))
        {
            value = age.yearToDay(ageInYears);
            displayResults(value, choice);
        }
        else if (choice.equalsIgnoreCase("Hours"))
        {
            value = age.yearToHour(ageInYears);
            displayResults(value, choice);
        }
        else if (choice.equalsIgnoreCase("Seconds"))
        {
            value = age.yearToSecond(ageInYears);
            displayResults(value, choice);
        }
        else
        {
            age.toError();
        }
    }

    public static String getChoice(){
        String choice;

        System.out.println("Would you like to know how old you are in months, days, hours, or seconds?");
        choice = input.nextLine();

        return choice;
    }
    //capture user age
    public static double getUserAge()
    {
        double ageInYears;

        System.out.println("In years, how old are you?");
        ageInYears = input.nextDouble();

        return ageInYears;
    }

    //display results in console
    public static void displayResults(double value, String choice)
    {
        //Output calculated age
        System.out.println("Your age converted to " + choice + ": " + value);
    }

}

class AgeConverter{
    public static double yearToMonth(double ageInYears)
    {
        double ageInMonths;

        ageInMonths = 12 * ageInYears;

        return ageInMonths;

    }

    public static double yearToDay(double ageInYears)
    {
        double ageInDays;

        ageInDays = 365 * ageInYears;

        return ageInDays;

    }

    public static double yearToHour(double ageInDays)
    {
        double ageInHours;

        ageInHours = 24 * yearToDay(ageInDays);

        return ageInHours;

    }

    public static double yearToSecond(double ageInHours)
    {
        double ageInSeconds;

        ageInSeconds = 3600 * yearToHour(ageInHours);

        return ageInSeconds;

    }

    public static void toError()
    {
        System.out.println("Error, test more more more more");
    }
}