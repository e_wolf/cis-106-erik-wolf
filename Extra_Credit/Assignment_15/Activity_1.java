/**
 * Created by Erik on 12/13/2017.
 */
/*
Create a program that asks the user what shape they would like to calculate the area for.
Use if/else conditional statements to determine their selection and then gather the appropriate input and calculate and display the area of the shape.
Perform all area calculations using a ShapeArea class that has separate methods to calculate and return the area for different shapes.
Include data validation in the class and error handling in the main program.
*/

import java.util.Scanner;

class Main {
    public static Scanner input = new Scanner(System.in);

    public static void main(String[] args)
    {
        ShapeArea area = new ShapeArea();
        String choice;

        System.out.println("Would you like to determine the area of a Circle, Triangle, Trapezoid, or Ellipse?");
        choice = input.nextLine();

        if (choice.equalsIgnoreCase("Circle"))
        {
            area.toCircle();
        }
        else if (choice.equalsIgnoreCase("Triangle"))
        {
            area.toTriangle();
        }
        else if (choice.equalsIgnoreCase("Trapezum"))
        {
            area.toTrapezoid();
        }
        else if (choice.equalsIgnoreCase("Ellipse"))
        {
            area.toEllipse();
        }
        else
        {
            area.toError();
        }
    }
}

class ShapeArea
{

    public static Scanner input = new Scanner(System.in);

    public static void toCircle()
    {
        double radius;
        double answer;

        System.out.println("Please enter the radius of the circle:");
        radius = input.nextDouble();

        answer = Math.PI * Math.pow(radius, 2);

        System.out.println("The area of the circle is:" + answer);
    }

    //Area of Triangle with output
    public static void toTriangle()
    {
        double height;
        double base;
        double answer;

        System.out.println("Please enter the base of the triangle:");
        base = input.nextDouble();
        System.out.println("Please enter the height of the triangle:");
        height = input.nextDouble();

        answer = base * height *(double) 1 / 2;

        System.out.println("The area of the triangle is :" + answer);
    }

    //Area of Trapezoid with output
    public static void toTrapezoid()
    {
        double baseA;
        double baseB;
        double altitude;
        double answer;

        System.out.println("Please enter first base:");
        baseA = input.nextDouble();
        System.out.println("Please enter second base:");
        baseB = input.nextDouble();
        System.out.println("Please enter altitude:");
        altitude = input.nextDouble();

        answer = (baseA + baseB) / 2 * altitude;

        System.out.println("The area of the Trapezoid is :" + answer);
    }

    //Area of Ellipse with output
    public static void toEllipse()
    {
        double semiMinor;
        double semiMajor;
        double answer;

        System.out.println("Please enter the Semi-Major Axis:");
        semiMajor = input.nextDouble();
        System.out.println("Please enter the Semi-Minor Axis");
        semiMinor = input.nextDouble();

        answer = Math.PI * semiMajor * semiMinor;

        System.out.println("The area of the Ellipse is:" + answer);
    }

    //simple error message
    public static void toError()
    {
        System.out.println("Error, test more more more more");
    }
}
