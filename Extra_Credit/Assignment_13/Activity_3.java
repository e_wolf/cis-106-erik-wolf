/*
Create a program that asks the user for a line of comma-separated-values. 
It could be a sequence of test scores, names, or any other values. 
Use string functions/methods to parse the line and print out each item on a separate line. 
Remove commas and any leading or trailing spaces from each item when printed. 
Use separate subroutines/functions/methods to implement input, each type of processing, and output. 
Avoid global variables by passing parameters and returning results.
*/

import java.util.Scanner;

public class Assignment_13 {

    public static void main (String[] args) throws Exception
    {
       String str = Input();
       Output(str);
    }

    private static String Input()
    {
        Scanner input = new Scanner(System.in);

        System.out.println("Enter line of comma seperated values: ");
        String strValues = input.nextLine();

        return  strValues;
    }

    private static void Output(String str)
    {
        String[] newValues = str.split(",");


        for (String result : newValues)
        {
            System.out.println(result.trim());
        }
    }
}
