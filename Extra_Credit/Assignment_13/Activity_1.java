import java.util.Scanner;

/**
 *Create a program that asks the user for a line of text containing a first name and last name, such as Firstname Lastname. Use string functions/methods to parse the line and print out the name in the form last name, first initial, such as lastname, f.
 * Created by Erik on 12/13/2017.
 */
public class Assignment_13
{
    public static void main(String[] args) throws Exception
    {
        Scanner input = new Scanner(System.in);

        System.out.println("Enter a First and Last Name: ");
        String fullname = input.nextLine();

        String[] name = fullname.split(" ");

        System.out.print(name[1] + ", " + name[0].charAt(0) +".");
    }
}
