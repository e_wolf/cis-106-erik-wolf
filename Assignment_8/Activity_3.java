// Create a program that asks the user how old they are in years. Then ask the user if they would like to know how old they are in months, days, hours, or seconds. Use if/else conditional statements to display their approximate age in the selected timeframe. Use separate subroutines/functions/methods for input, processing, and output. Avoid global variables by passing parameters and returning results.

import java.util.*;
import java.lang.Math;
import java.util.Scanner;


public class Main 
{
	private static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args)
	{
		double age;
		int calculatedTimeFrame;
		double ageTimeFrame;
		
		String timeframe;
		
		age = ageInYears();
		timeframe = getTimeFrame();
		calculatedTimeFrame = switchTimeFrame(timeframe);
		ageTimeFrame = ageConversion(calculatedTimeFrame, age);
		
		displayResults(age, timeframe, ageTimeFrame);
		
	}
	
	// get user age
	private static double ageInYears()
	{
		double age;
		
		System.out.println("How old are you in years?");
		age = input.nextDouble();
		
		return age;
	}
	
	// get user's desired timeframe
	private static String getTimeFrame()
	{
		String timeframe;
		
		System.out.println("Would you like to know how old you are in Months, Days, Hours, or Seconds?");
		timeframe = input.next();
		
		return timeframe;
	}
	
	// switch, that accepts timeframe and catalogs timeframe
	private static int switchTimeFrame(String timeframe)
	{
		int calculatedTimeFrame;
		
		calculatedTimeFrame = 0;
		
		switch(timeframe.toLowerCase())
		{
			
			case "month": 
			case "months": 
				calculatedTimeFrame = 1;
				break;
			case "day": 
			case "days": 
				calculatedTimeFrame = 2;
				break;
			case "hour": 
			case "hours": 
				calculatedTimeFrame = 3;
				break;
			case "second": 
			case "seconds": 
				calculatedTimeFrame = 4;
				break;
			default: 
				System.out.println("ERROR");
				System.exit(0);
		}
		
		return calculatedTimeFrame;
	}
	
	// switch, accepts cataloged number and calls calculation funtions
	private static double ageConversion(int calculatedTimeFrame, double age)
	{
		double ageTimeFrame;
		
		ageTimeFrame = 0;
		
		switch(calculatedTimeFrame)
		{
			
			case 1: 
				ageTimeFrame = ageToMonths(age);
				break;
				
			case 2: 
				ageTimeFrame = ageToDays(age);
				break;
				
			case 3: 
				ageTimeFrame = ageToHours(age);
				break;
				
			case 4: 
				ageTimeFrame = ageToSeconds(age);
				break;
				
			default: 
				System.out.println("ERROR");
				System.exit(1);
		}
		
		return ageTimeFrame;
	}
	
	private static double ageToMonths(double age)
	{
		double ageTimeFrame;
		
		ageTimeFrame = age * 12;
		
		return ageTimeFrame;
	}
	
	private static double ageToDays(double age)
	{
		double ageTimeFrame;
		
		ageTimeFrame = age * 365;
		
		return ageTimeFrame;
	}
	
	private static double ageToHours(double age)
	{
		double ageTimeFrame;
		
		ageTimeFrame = age * 8765.81278;
		
		return ageTimeFrame;
	}
	
	private static double ageToSeconds(double age)
	{
		double ageTimeFrame;
		
		ageTimeFrame = age * 31556926;
		
		return ageTimeFrame;
	}

	//displays results
	public static void displayResults(double age, String timeframe, double ageTimeFrame)
	{
		System.out.println("You are " + age + " years old, but you are also " + ageTimeFrame + " " + timeframe + " old.");
	}
}