// Create a program that asks the user what shape they would like to calculate the area for. Use if/else conditional statements to determine their selection and then gather the appropriate input and calculate and display the area of the shape. Use separate subroutines/functions/methods for input, processing, and output. Avoid global variables by passing parameters and returning results.


// Review Wikipedia: Is functions. If your programming language supports it, update one or more of the programs above to include input validation for all numeric input.

import java.util.*;
import java.lang.Math;
import java.util.Scanner;
import java.util.InputMismatchException;


public class Main 
{
	private static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args)
	{
		String shape;
		double area;
		
		shape = getShape();
		area = setArea(shape);
		displayResult(shape, area);
	}
    
	//gets desired shape from user
	private static String getShape()
	{
		String shape;
		
		System.out.println("Which shape would you like to know the Area for?");
		System.out.println("1. Triangle");
		System.out.println("2. Square");
		System.out.println("3. Rectangle");
		System.out.println("4. Parallelogram");
		System.out.println("5. Trapezoid");
		System.out.println("6. Circle");
		System.out.println("7. Ellipse");
		
		shape = input.nextLine();
		
		return shape;
	}

	//switch case that assigns gets and assigns parameters to areaOf_____() 
	private static double setArea(String shape)
	{
		double area;
		double height;
		double width;
		double side;
		double semiMinor;
    	double semiMajor;
    	double baseA;
		double baseB;
		double radius;
		
		area = 0;
		
		switch(shape.toLowerCase())
		{
			case "1":
			case "triangle":
			case "triangles":
				baseA = getBaseA();
				height = getHeight();
				area = areaOfTriangle(baseA, height);
				break;
			
			case "2":
			case "square":
			case "squares":
				side = getSide();
				area = areaOfSquare(side);
				break;
				
			case "3":
			case "rectangle":
			case "rectangles":
				width = getWidth();
				height = getHeight();
				area = areaOfRectangle(width, height);
				break;
				
			case "4":
			case "parallelogram":
			case "parallelograms":
				baseA = getBaseA();
				height = getHeight();
			 	area = areaOfParallelogram(baseA, height);
			 	break;
			
			case "5":
			case "trapezoid":
			case "trapezoids":
				baseA = getBaseA();
				baseB = getBaseB();
				height = getHeight();
				area = areaOfTrapezoid(baseA, baseB, height);
				break;
				
			case "6":
			case "circle":
			case "circles":
			 	radius = getRadius();
			 	area = areaOfCircle(radius);
			 	break;
				
			case "7":
			case "ellipse":
			case "ellipses":
				semiMajor = getSemiMajor();
				semiMinor = getSemiMinor();
			 	area = areaOfEllipse(semiMajor, semiMinor);
			 	break;
				
			default :
				System.out.println("ERROR");
				System.exit(0);
		}
		
		return area;
	}
	
	// do..while.. checking for valid data type Double with input, no strings accepted
	private static double getBaseA()
	{
		double baseA;
		boolean isDouble;
		
		baseA = 0;
		
		do	{
			System.out.println("Please enter the base:");
			
			if (input.hasNextDouble())
			{
				baseA = input.nextDouble();
				isDouble = true;
			}	
		
			else{
				System.out.println("I do not understand!");
				isDouble = false;
				input.next();
			}
		
		}	while(!(isDouble));
		
		return baseA;	
	}
	
	private static double getBaseB()
	{
		double baseB;
		boolean isDouble;
		
		baseB = 0;
		
		do	{
			System.out.println("Please enter the second base:");
			
			if (input.hasNextDouble())
			{
				baseB = input.nextDouble();
				isDouble = true;
			}	
		
			else{
				System.out.println("I do not understand!");
				isDouble = false;
				input.next();
			}
		
		}	while(!(isDouble));

		
		return baseB;
	}
	
	private static double getHeight()
	{
		double height;
		boolean isDouble;
		
		height = 0;
		
		do	{
			System.out.println("Please enter the height:");
			
			if (input.hasNextDouble())
			{
				height = input.nextDouble();
				isDouble = true;
			}	
		
			else{
				System.out.println("I do not understand!");
				isDouble = false;
				input.next();
			}
		
		}	while(!(isDouble));
		
		return height;
	}
	
	private static double getSide()
	{
		double side;
		boolean isDouble;
		
		side = 0;
		
		do	{
			System.out.println("Please enter length of side:");
			
			if (input.hasNextDouble())
			{
				side = input.nextDouble();
				isDouble = true;
			}	
		
			else{
				System.out.println("I do not understand!");
				isDouble = false;
				input.next();
			}
		
		}	while(!(isDouble));
		
		return side;
	}
	
	private static double getWidth()
	{
		double width;
		boolean isDouble;
		
		width = 0;
		
		do	{
			System.out.println("Please enter the width:");
			
			if (input.hasNextDouble())
			{
				width = input.nextDouble();
				isDouble = true;
			}	
		
			else{
				System.out.println("I do not understand!");
				isDouble = false;
				input.next();
			}
		
		}	while(!(isDouble));
		
		return width;
	}
	
	private static double getSemiMajor()
	{
		double semiMajor;
		boolean isDouble;
		
		semiMajor = 0;
		
		do	{
			System.out.println("Please enter the Semi Major:");
			
			if (input.hasNextDouble())
			{
				semiMajor = input.nextDouble();
				isDouble = true;
			}	
		
			else{
				System.out.println("I do not understand!");
				isDouble = false;
				input.next();
			}
		
		}	while(!(isDouble));

		return semiMajor;
	}
	
	private static double getSemiMinor()
	{
		double semiMinor;
		boolean isDouble;
		
		semiMinor = 0;
		
		do	{
			System.out.println("Please enter the Semi Minor:");
			
			if (input.hasNextDouble())
			{
				semiMinor = input.nextDouble();
				isDouble = true;
			}	
		
			else{
				System.out.println("I do not understand!");
				isDouble = false;
				input.next();
			}
		
		}	while(!(isDouble));

		return semiMinor;
	}
	
	private static double getRadius()
	{
		double radius;
		boolean isDouble;
		
		radius = 0;
		
		do	{
			System.out.println("Please enter the radius:");
			
			if (input.hasNextDouble())
			{
				radius = input.nextDouble();
				isDouble = true;
			}	
		
			else
			{
				System.out.println("I do not understand!");
				isDouble = false;
				input.next();
			}
		
		}	while(!(isDouble));

        return radius;
	}
	
	private static double areaOfTriangle(double base, double height)
	{
		double area;
		
		area = base * height *(double) 1 / 2;
		
		return area;
	}
	
	private static double areaOfCircle(double radius)
	{
		double area;
		
		area = Math.PI * Math.pow(radius, 2);
		
		return area;
	}
	
	private static double areaOfSquare(double side)
	{
		double area;
		
		area = Math.pow(side, 2);
		
		return area;
	}
	
	private static double areaOfRectangle(double width, double height)
	{
		double area;
		
		area = width * height;
		
		return area;
	}
	
	private static double areaOfParallelogram(double baseA, double height)
	{
		double area;
		
		area = baseA * height;
		
		return area;
	}
	
	private static double areaOfTrapezoid(double height, double baseA, double baseB)
	{
		double area;
		
		area = (baseA + baseB) / 2 * height;
		
		return area;
	}
	
	private static double areaOfEllipse(double semiMinor, double semiMajor)
    {
    	double area;
    	
    	area = Math.PI * semiMajor * semiMinor;
    	
    	return area;
    }
    
	//output results
	public static void displayResult(String shape, double area)
	{
		System.out.println("Area: " + area);
	}
}