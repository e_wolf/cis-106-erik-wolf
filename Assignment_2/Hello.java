class main {
    private static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args) {
        String name;
        
        System.out.println("Please enter your name:");
        name = input.nextLine();
        System.out.println("Hello " + name);
    }
}
