import java.util.*;
import java.lang.Math;
import java.util.Scanner;

class JavaApplication {
    private static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args) {
        // Create a program to prompt the user for hours and rate per hour and then compute gross pay
        int hourlyRate;
        int hoursWorked;
        int grossPay;
        
        // prompt user input
        System.out.println("Enter hourly rate: ");
        hourlyRate = input.nextInt();
        System.out.println("Enter hours worked: ");
        hoursWorked = input.nextInt();
        
        // calculate input from user to determine Gross Pay (hours * rate)
        grossPay = hourlyRate * hoursWorked;
        
        // Output grossPay
        System.out.println("Rate: $" + hourlyRate + "/hr, Hours: " + hoursWorked + "hrs, Gross Pay: $" + grossPay);
    }
}
