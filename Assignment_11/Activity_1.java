  // Create a program that asks the user to enter grade scores. Start by asking the user how many scores they would like to enter. Then use a loop to request each score and add it to a static (fixed-size) array. After the scores are entered, calculate and display the high, low, and average for the entered scores. 

import java.util.*;
import java.lang.Math;
import java.util.Scanner;

class Main {
    private static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args) 
    {
    	int[] gradeCount = new int[100];
        int size;
        int average;
        int high;
        int low;
        
        size = GetSize();
        GradeBook(gradeCount, size);
        average = CalculateAverage(gradeCount, size);
        high = CalculateHigh(gradeCount, size);
        low = CalculateLow(gradeCount, size);
        Output(gradeCount, average, high, low);
    }

	private static int GetSize()
	{
		int size;
		
		System.out.println("How many Grades need to be entered?");
		size = input.nextInt();
		
		return size;
	}
    private static int CalculateAverage(int[] gradeCount, int size) 
    {
        int average;
        int total;
        int i;
        
        total = 0;
        
        for (i = 0 ; i <= size ; i += 1) 
        {
            total = total + gradeCount[i];
        }
        
        average = (total / size);
        
        return average;
    }

    private static int CalculateHigh(int[] gradeCount, int size) 
    {
        int i;
        int high;
        
        high = gradeCount[0];
        
        for (i = 1 ; i <= size - 1 ; i += 1) 
        {
            if (gradeCount[i] > high) 
            {
                high = gradeCount[i];
            }
        }
        
        return high;
    }

    private static int CalculateLow(int[] gradeCount, int size) 
    {
        int i;
        int low;
        
        low = gradeCount[0];
        
        for (i = 1 ; i <= size - 1 ; i += 1) {
            if (gradeCount[i] < low) 
            {
                low = gradeCount[i];
            }
        }
        
        return low;
    }

    private static void GradeBook(int[] gradeCount, int size) 
    {
        int i;
        
        System.out.println("Enter Grades: ");
        
        for (i = 0 ; i <= size - 1 ; i += 1) 
        {
            gradeCount[i] = input.nextInt();
        }
    }

    private static void Output(int[] gradeCount, int average, int high, int low) 
    {
        System.out.println("Average Test Score: " + average);
        System.out.println("Highest Test Score: " + high);
        System.out.println("Lowest Test Score: " + low);
    }
}