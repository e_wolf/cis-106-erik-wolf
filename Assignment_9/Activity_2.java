import java.util.*;
import java.lang.Math;
import java.util.Scanner;

class Main 
{
    private static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args) 
    {
        // Create a program that uses a loop to generate a list of multiplication expressions for a given value. Ask the user to enter the value and the number of expressions to be displayed. For example, a list of three expressions for the value 1 would be:
        int userValue;
        
        userValue = getValue();
        int numberOfExpression;
        
        numberOfExpression = getExpressionValue();
        forLoop(userValue, numberOfExpression);
        whileLoop(userValue, numberOfExpression);
    }

    private static void forLoop(int userValue, int numberOfExpression) 
    {
        int operation;
        int i;
        
        for (i = 1 ; i <= numberOfExpression ; i += 1) 
        {
            operation = i * userValue;
            Output(i, userValue, operation);
        }
    }

    private static int getExpressionValue() 
    {
        int numberOfExpression;
        
        System.out.println("Enter the the amount of expressions desired:");
        numberOfExpression = input.nextInt();
        
        return numberOfExpression;
    }

    private static int getValue() 
    {
        int userValue;
        
        System.out.println("Enter Value:");
        userValue = input.nextInt();
        
        return userValue;
    }

    private static void Output(int i, int userValue, int operation) 
    {
        System.out.println(i + " * " + userValue + " = " + operation);
    }

    private static void whileLoop(int userValue, int numberOfExpression) 
    {
        int i;
        int operation;
        
        i = 1;
        while (i <= numberOfExpression) 
        {
            operation = i * userValue;
            Output(i, userValue, operation);
            i = i + 1;
        }
    }
}