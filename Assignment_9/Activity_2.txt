Module main()
    // Create a program that uses a loop to generate a list of multiplication expressions for a given value. Ask the user to enter the value and the number of expressions to be displayed. For example, a list of three expressions for the value 1 would be:
    Declare Real userValue
    
    Set userValue = getValue()
    Declare Real numberOfExpression
    
    Set numberOfExpression = getExpressionValue()
    Call forLoop(userValue, numberOfExpression)
    Call whileLoop(userValue, numberOfExpression)
End Module

Module forLoop(Real userValue, Real numberOfExpression)
    Declare Real operation
    Declare Real i
    
    For i = 1 To numberOfExpression
        Set operation = (i + 1) * userValue
        Call Output(i, userValue, operation)
    End For
End Module

Function Integer getExpressionValue()
    Declare Real numberOfExpression
    
    Display "Enter the the amount of expressions desired:"
    Input numberOfExpression
    
    Return numberOfExpression
End Function

Function Real getValue()
    Declare Real userValue
    
    Display "Enter Value:"
    Input userValue
    
    Return userValue
End Function

Module Output(Real i, Real userValue, Real operation)
    Display i, " * ", userValue, " = ", operation
End Module

Module whileLoop(Real userValue, Real numberOfExpression)
    Declare Real i
    Declare Real operation
    
    Set i = 1
    While i <= numberOfExpression
        Set operation = i * userValue
        Call Output(i, userValue, operation)
        Set i = i + 1
    End While
End Module
